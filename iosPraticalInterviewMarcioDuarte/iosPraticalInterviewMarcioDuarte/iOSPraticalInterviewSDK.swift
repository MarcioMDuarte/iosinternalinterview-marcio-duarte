//
//  iOSPraticalInterviewSDK.swift
//  [iOS Practical Interview] - Márcio Duarte
//
//  Created by Andre Duarte on 25/01/2019.
//  Copyright © 2019 Márcio Duarte. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper


struct iOSPraticalInterviewSDK
{
    static var apiKey = "f9cc014fa76b098f9e82f1c288379ea1"
    
    private static var alamofireManager: Alamofire.SessionManager = {
        // Create custom manager
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        let manager = Alamofire.SessionManager()
        manager.session.configuration.timeoutIntervalForResource = 20
        
        return manager
    }()
    
    
    
    private typealias Endpoint = (url: String, method: HTTPMethod)
    private struct Endpoints
    {
        static let baseURL = "https://api.flickr.com/services/rest/"
        static func getSearchPhotos () -> Endpoint{
            let endpoint: Endpoint = (url: baseURL + "?method=flickr.photos.search", method: .get)
            return endpoint
        }
        
        static func getSizes () -> Endpoint{
            let endpoint: Endpoint = (url: baseURL + "?method=flickr.photos.getSizes", method: .get)
            return endpoint
        }
        
    }
    
    struct service
    {
        static func getPhotoSearch(tags tag: String, page page: Int, format format: String, noJsonCallBack noJsonCallBack: Int,didFinish: @escaping(PhotosSearch?, Bool) -> Void)
        {
            var components = APICallComponents(headers: nil, parameters: nil, body: nil)
            
            let parameters = [
                "api_key" : apiKey,
                "tags" : tag,
                "page" : page,
                "format" : format,
                "nojsoncallback" : noJsonCallBack,
                ] as [String : Any]
            components.add(parameters: parameters)
            iOSPraticalInterviewSDK.call(Endpoints.getSearchPhotos(), withComponents: components) { (PhotosSearch, success) in
                didFinish(PhotosSearch, success)
            }
        }
        
        static func getSizes(photoid photo_id: String, format format: String, noJsonCallBack noJsonCallBack: Int,didFinish: @escaping(ImagesSizes?, Bool) -> Void)
        {
            var components = APICallComponents(headers: nil, parameters: nil, body: nil)
            
            let parameters = [
                "api_key" : apiKey,
                "photo_id" : photo_id,
                "format" : format,
                "nojsoncallback" : noJsonCallBack,
                ] as [String : Any]
            components.add(parameters: parameters)
            iOSPraticalInterviewSDK.call(Endpoints.getSizes(), withComponents: components) { (ImageSizes, success) in
                didFinish(ImageSizes, success)
            }
        }
    }
    
    private static func call <T: Mappable> (_ endpoint: Endpoint, withComponents components: APICallComponents, didReceive: @escaping ( T?, Bool)->Void)
    {
        var parameters: [String:Any]?
        switch endpoint.method {
        case .get:
            parameters = components.parameters
        case .post, .put:
            parameters = components.body
        default:
            break
        }
        
        
        iOSPraticalInterviewSDK.alamofireManager.request(endpoint.url, method: endpoint.method , parameters: parameters, encoding:  URLEncoding.default, headers: components.headers)
            .validate(statusCode: 200..<400)
            .responseJSON {
                response in
                
                print("\nAlamofire.request ===>> \(response.request?.httpMethod ?? "N/A") \(response.request?.url?.absoluteString ?? "N/A")")
                print("\nAlamofire.request response.description ===>> \(response.description)")
                
                switch response.result
                {
                case .success:
                    if let json = response.result.value as? [String:Any], let object = T(JSON: json)
                    {
                        print(object)
                        DispatchQueue.main.async {
                            didReceive(object, true)
                        }
                    }
                case .failure:
                    return
                        DispatchQueue.main.async {
                            didReceive(nil, false)
                    }
                }
        }
    }
    
}
