//
//  ArrayOfSizes.swift
//  [iOS Practical Interview] - Márcio Duarte
//
//  Created by Andre Duarte on 28/01/2019.
//  Copyright © 2019 Márcio Duarte. All rights reserved.
//

import Foundation
import ObjectMapper

class ArrayOfSizes: Mappable
{
    var label: String? = nil
    var width: String? = nil
    var height: String? = nil
    var source: String? = nil
    var url: String? = nil
    var media: String? = nil
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        self.label <- map["label"]
        self.width <- map["width"]
        self.height <- map["height"]
        self.source <- map["source"]
        self.url <- map["url"]
        self.media <- map["media"]
       
    }
}
