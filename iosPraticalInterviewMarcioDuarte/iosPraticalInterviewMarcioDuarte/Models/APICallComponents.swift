//
//  APICallComponents.swift
//  [iOS Practical Interview] - Márcio Duarte
//
//  Created by Andre Duarte on 25/01/2019.
//  Copyright © 2019 Márcio Duarte. All rights reserved.
//

import Foundation

struct APICallComponents {
    var headers: [String:String]?
    var parameters: [String:Any]?
    var body: [String:Any]?
    
    mutating func add (headers: [String:String])
    {
        self.headers = headers
    }
    mutating func add (parameters: [String:Any])
    {
        self.parameters = parameters
    }
    mutating func add (body: [String:Any])
    {
        self.body = body
    }
}
