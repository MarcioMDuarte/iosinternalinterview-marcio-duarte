//
//  Photos.swift
//  [iOS Practical Interview] - Márcio Duarte
//
//  Created by Andre Duarte on 25/01/2019.
//  Copyright © 2019 Márcio Duarte. All rights reserved.
//

import Foundation
import ObjectMapper

class PhotosSearch: Mappable
{
    
    var photos: Photos? = nil
    var stat: String? = nil
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        self.photos <- map["photos"]
        self.stat <- map["stat"]
    }
}
