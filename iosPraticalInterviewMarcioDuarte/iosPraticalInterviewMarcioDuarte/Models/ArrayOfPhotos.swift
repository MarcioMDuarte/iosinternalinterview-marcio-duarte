//
//  ArrayPhotos.swift
//  [iOS Practical Interview] - Márcio Duarte
//
//  Created by Andre Duarte on 25/01/2019.
//  Copyright © 2019 Márcio Duarte. All rights reserved.
//

import Foundation
import ObjectMapper

class ArrayOfPhotos: Mappable
{
    var id: String? = nil
    var owner: String? = nil
    var secret: String? = nil
    var server: String? = nil
    var farm: Int = 0
    var title: String? = nil
    var ispublic: Int = 0
    var isfriend: Int = 0
    var isfamily: Int = 0
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        self.id <- map["id"]
        self.owner <- map["id"]
        self.secret <- map["secret"]
        self.server <- map["server"]
        
        self.farm <- map["farm"]
        self.title <- map["title"]
        self.ispublic <- map["ispublic"]
        self.isfriend <- map["isfriend"]
        self.isfamily <- map["isfamily"]
    }
}
