//
//  ImagesSizes.swift
//  [iOS Practical Interview] - Márcio Duarte
//
//  Created by Andre Duarte on 28/01/2019.
//  Copyright © 2019 Márcio Duarte. All rights reserved.
//

import Foundation
import ObjectMapper

class ImagesSizes: Mappable
{
    
    var sizes: Sizes? = nil
    var stat: String? = nil
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        self.sizes <- map["sizes"]
        self.stat <- map["stat"]
    }
}
