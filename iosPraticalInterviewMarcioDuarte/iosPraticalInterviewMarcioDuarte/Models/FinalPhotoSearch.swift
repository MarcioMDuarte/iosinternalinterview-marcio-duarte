//
//  FinalPhotoSearch.swift
//  [iOS Practical Interview] - Márcio Duarte
//
//  Created by Andre Duarte on 28/01/2019.
//  Copyright © 2019 Márcio Duarte. All rights reserved.
//

import Foundation
import RealmSwift

class FinalPhotoSearch: Object
{
    @objc dynamic var id = UUID().uuidString
    @objc dynamic var title: String? = nil
    @objc dynamic var source: String? = nil
    @objc dynamic var largeImage: String? = nil
    
    // MARK: - Realm management
    override static func primaryKey() -> String? {
        return "id"
    }
}
