//
//  Sizes.swift
//  [iOS Practical Interview] - Márcio Duarte
//
//  Created by Andre Duarte on 28/01/2019.
//  Copyright © 2019 Márcio Duarte. All rights reserved.
//

import Foundation
import ObjectMapper

class Sizes: Mappable
{
    var canblog: Int = 0
    var canprint: Int = 0
    var candownload: Int = 0
    var size: [ArrayOfSizes] = []
    
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        self.canblog <- map["page"]
        self.canprint <- map["pages"]
        self.candownload <- map["perpage"]
        self.size <- map["size"]
    }
}
