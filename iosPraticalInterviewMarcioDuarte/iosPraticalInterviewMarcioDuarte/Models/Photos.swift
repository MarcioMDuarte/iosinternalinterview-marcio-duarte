//
//  PhotosSearch.swift
//  [iOS Practical Interview] - Márcio Duarte
//
//  Created by Andre Duarte on 25/01/2019.
//  Copyright © 2019 Márcio Duarte. All rights reserved.
//

import Foundation
import ObjectMapper
import RealmSwift

class Photos: Mappable
{
    var page: Int = 0
    var pages: Int = 0
    var perpage: Int = 0
    var total: String? = nil
    var photos: [ArrayOfPhotos] = []
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        self.page <- map["page"]
        self.pages <- map["pages"]
        self.perpage <- map["perpage"]
        self.total <- map["total"]
        self.photos <- map["photo"]
    }
}
