//
//  ViewController.swift
//  [iOS Practical Interview] - Márcio Duarte
//
//  Created by Andre Duarte on 25/01/2019.
//  Copyright © 2019 Márcio Duarte. All rights reserved.
//

import UIKit
import Kingfisher
import RealmSwift

class PhotoGalleryViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UISearchBarDelegate, photoCollectionProtocol {
    

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    private let sectionInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
    var photos = [ArrayOfPhotos]()
    var sizes = [ArrayOfSizes]()
    var finalPhotoSearch = [FinalPhotoSearch]()
    var pagination: Int = 1
    var totalPages: Int = 0
     let myGroup = DispatchGroup()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.tabBarItem.title = "Image gallery app"
        let returnValue: String? = UserDefaults.standard.object(forKey: "searchTitle") as? String
        self.searchBar.text = returnValue
        self.searchBar.placeholder = "Search"
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.DismissKeyboard))
        self.view.addGestureRecognizer(tapGesture)
        
        self.collectionView.register(UINib(nibName: PhotoCollectionViewCell.nibName, bundle: nil), forCellWithReuseIdentifier: PhotoCollectionViewCell.reuseIdentifier)
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.isUserInteractionEnabled = true
        self.collectionView.isPagingEnabled = true
        
        
        self.collectionView.refreshControl = UIRefreshControl()
        self.collectionView.refreshControl?.attributedTitle = NSAttributedString(string: "")
        self.collectionView.refreshControl?.addTarget(self, action: #selector(self.getRefreshPhotos), for: .valueChanged)
        
        let realm = try! Realm()
        var finalArray = Array(realm.objects(FinalPhotoSearch.self))
        if !finalArray.isEmpty
        {
            self.finalPhotoSearch = finalArray
            self.pagination = Int(round(Double(self.finalPhotoSearch.count / 100)))
            self.collectionView.reloadData()
        }
        else
        {
            self.getPhotosWithPagination(withSearch: "", andPagination: 1)
//            self.getPhotos(withSearch: "")
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if self.finalPhotoSearch.count > 0
        {
            self.collectionView.backgroundView = nil
            return self.finalPhotoSearch.count
        }
        else {
            let rect = CGRect(x: 0,
                              y: 0,
                              width: self.collectionView.bounds.size.width,
                              height: self.collectionView.bounds.size.height)
            let noDataLabel: UILabel = UILabel(frame: rect)
            noDataLabel.text = "The Photo Collection is empty."
            noDataLabel.textAlignment = .center
            noDataLabel.textColor = UIColor.gray
            noDataLabel.sizeToFit()
            
            self.collectionView.backgroundView = noDataLabel
            
            return 0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let photoCell = self.collectionView.dequeueReusableCell(withReuseIdentifier: PhotoCollectionViewCell.reuseIdentifier, for: indexPath) as! PhotoCollectionViewCell
        photoCell.delegate = self
        let photo = self.finalPhotoSearch[indexPath.row]
        photoCell.index = photo
        photoCell.titleLabel.text = photo.title
        photoCell.imageView.kf.setImage(with: URL(string: photo.source!))
        
        
        if self.finalPhotoSearch.count-1 == indexPath.row && self.pagination <= self.totalPages
        {
            self.pagination = self.pagination + 1
            self.getPhotosWithPagination(withSearch: self.searchBar.text!, andPagination: self.pagination)
        }
        
        return photoCell
    }

    func selectCell(photo: FinalPhotoSearch) {
        let alert = CustomAlert(title: photo.title!, image: photo.largeImage!)
        alert.show(animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionWidth = self.collectionView.bounds.width/2 - 10.0
        return CGSize(width: collectionWidth, height: collectionWidth)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.finalPhotoSearch = []
        let realm = try! Realm()
        try! realm.write {
            realm.deleteAll()
        }
        self.pagination = 1
        self.getPhotosWithPagination(withSearch: self.searchBar.text!, andPagination: self.pagination)
        self.view.endEditing(true)
    }
    
    @IBAction func CancelSearchAndClearResults(_ sender: Any)
    {
        let realm = try! Realm()
        try! realm.write {
            realm.deleteAll()
        }
        self.getPhotosWithPagination(withSearch: "", andPagination: self.pagination)
        //self.getPhotos(withSearch: "")
        self.searchBar.text = ""
        UserDefaults.standard.removeObject(forKey:"searchTitle")
        self.view.endEditing(true)
    }
    
    @objc func DismissKeyboard()
    {
        self.view.endEditing(true)
    }
    
    
    func getPhotosWithPagination(withSearch tag: String, andPagination pagination: Int)
    {
        iOSPraticalInterviewSDK.service.getPhotoSearch(tags: tag, page: pagination, format: "json", noJsonCallBack: 1) { (PhotoSearch, Success) in
            if Success
            {
                if let photos = PhotoSearch?.photos?.photos, let totalPages = PhotoSearch?.photos?.pages
                {
                    UserDefaults.standard.set(self.searchBar.text!, forKey: "searchTitle")
                    self.photos = []
                    self.totalPages = totalPages
                    self.photos = photos
                    
                    let realm = try! Realm()
                    try! realm.write
                    {
                        realm.add(self.finalPhotoSearch, update: true)
                    }
                }
                else
                {
                    self.photos = []
                    self.finalPhotoSearch = []
                    self.collectionView.refreshControl?.endRefreshing()
                    self.collectionView.reloadData()
                }
                
                for photo in self.photos
                {
                    self.myGroup.enter()
                    iOSPraticalInterviewSDK.service.getSizes(photoid: photo.id!, format: "json", noJsonCallBack: 1, didFinish: { (imageSizes, success) in
                        if Success
                        {
                            if let sizes = imageSizes?.sizes?.size
                            {
                                self.sizes = []
                                self.sizes = sizes
                                let photoToAray = FinalPhotoSearch()
                                for size in self.sizes
                                {
                                    
                                    if size.label == "Large Square"
                                    {
                                        photoToAray.title = photo.title
                                        photoToAray.source = size.source
                                    }
                                    if size.label == "Large"
                                    {
                                        photoToAray.largeImage = size.source
                                        self.finalPhotoSearch.append(photoToAray)

                                    }
                                }
                                
                            }
                        }
                        self.myGroup.leave()
                    })
                    
                }
                
            }
            
            self.myGroup.notify(queue: .main) {
                self.collectionView.reloadData()
                let realm = try! Realm()
                try! realm.write
                {
                    realm.add(self.finalPhotoSearch, update: true)
                }
            }
        }
    }
    
    @objc func getRefreshPhotos()
    {
        var tag = ""
        self.pagination = 1
        if let tagText = self.searchBar.text
        {
            tag = tagText
        }
        iOSPraticalInterviewSDK.service.getPhotoSearch(tags: tag, page: 1, format: "json", noJsonCallBack: 1) { (PhotoSearch, Success) in
            if Success
            {
                if let photos = PhotoSearch?.photos?.photos
                {
                    self.photos = []
                    self.finalPhotoSearch = []
                    self.photos = photos
                }
                else
                {
                    self.photos = []
                    self.finalPhotoSearch = []
                    self.collectionView.refreshControl?.endRefreshing()
                    self.collectionView.reloadData()
                }
                
                for photo in self.photos
                {
                    self.myGroup.enter()
                    iOSPraticalInterviewSDK.service.getSizes(photoid: photo.id!, format: "json", noJsonCallBack: 1, didFinish: { (imageSizes, success) in
                        if Success
                        {
                            if let sizes = imageSizes?.sizes?.size
                            {
                                self.sizes = []
                                self.sizes = sizes
                                let photoToAray = FinalPhotoSearch()
                                for size in self.sizes
                                {
                                    
                                    if size.label == "Large Square"
                                    {
                                        
                                        photoToAray.title = photo.title
                                        photoToAray.source = size.source
                                        
                                    }
                                    if size.label == "Large"
                                    {
                                        photoToAray.largeImage = size.source
                                        self.finalPhotoSearch.append(photoToAray)
                                        let realm = try! Realm()
                                        try! realm.write
                                        {
                                            realm.add(self.finalPhotoSearch, update: true)
                                        }
                                    }
                                }
                            }
                        }
                        self.myGroup.leave()
                    })
                }
            }
            self.myGroup.notify(queue: .main) {
                self.collectionView.refreshControl?.endRefreshing()
                self.collectionView.reloadData()
                let realm = try! Realm()
                try! realm.write
                {
                    realm.add(self.finalPhotoSearch, update: true)
                }
            }
        }
    }
}

