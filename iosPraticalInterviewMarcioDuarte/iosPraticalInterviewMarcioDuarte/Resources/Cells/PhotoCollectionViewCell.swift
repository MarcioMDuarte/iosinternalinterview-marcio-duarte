//
//  PhotoCollectionViewCell.swift
//  [iOS Practical Interview] - Márcio Duarte
//
//  Created by Andre Duarte on 25/01/2019.
//  Copyright © 2019 Márcio Duarte. All rights reserved.
//

import UIKit
protocol photoCollectionProtocol
{
    func selectCell(photo: FinalPhotoSearch)
    
}

class PhotoCollectionViewCell: UICollectionViewCell
{

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    var index: FinalPhotoSearch?
    
    var delegate: photoCollectionProtocol?
    
    static let nibName = "PhotoCollectionViewCell"
    static let reuseIdentifier = "PhotoCollectionViewCell"
    
    @IBAction func selectCell(_ sender: Any)
    {
        self.delegate?.selectCell(photo: self.index!)
    }
    
    
    
}
